# Welcome!

<a href="https://git.tayari.gg/tayari/Resume/-/raw/master/resume.pdf?inline=true"><img src="https://img.shields.io/badge/Resume (PDF)-%23E01F3D.svg?&style=for-the-badge&logoColor=white"/></a>
<a href="https://www.linkedin.com/in/daylamtayari"><img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white"/></a>

### <a href="#skills">Scroll down (or click here) to check out my various projects and skills!</a>

**I am Daylam Tayari, a cybersecurity college student currently going to Arizona State University and currently based in Phoenix, Arizona, USA though originally from Paris, France.**  
**I am fully bilingual in both French (natively) and English.**  
  
**In my free time, I love working on various open source projects, enriching my cybersecurity skills and being an active participant in the Linux community.** 

**I am also an avid open source and privacy advocate, helping others discover new tools and introducing them to ways to better protect their personal privacy.**
  
**I am active in local group and enthusiast communities, being an active member of my universities cybersecurity club where I compete in team cybersecurity competitions with, my universities Linux User's Group where we regularly host presentation, discuss various Linux and open source topics and introduce newcomers to Linux and its various affinities.**  
**I also regularly meet up with a local hacking enthusiast group, PHX2600, to discuss and attend presentations on a variety of cybersecurity and general technology topics.**
  

### Feel free to reach out to me by email:

<a href="mailto:daylamtayari@tayari.gg"><img src="https://img.shields.io/badge/daylamtayari@tayari.gg-%238B89CC.svg?&style=for-the-badge&logo=protonmail&logoColor=white"/></a> 
   
   
## Skills:

### Programming Languages:

![Java Badge](https://img.shields.io/badge/Java-007396?style=for-the-badge&labelColor=black&logo=java&logoColor=white)
![C++ Badge](https://img.shields.io/badge/C++-00599C?style=for-the-badge&labelColor=black&logo=cplusplus&logoColor=white)
![C Badge](https://img.shields.io/badge/C-A8B9CC?style=for-the-badge&labelColor=black&logo=c&logoColor=white)
![Python Badge](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&labelColor=black&logo=python&logoColor=white)
![JavaScript Badge](https://img.shields.io/badge/-Javascript-F0DB4F?style=for-the-badge&labelColor=black&logo=javascript&logoColor=white)

### Miscellaneous Languages:

![SQL Badge](https://img.shields.io/badge/SQL-003B57?style=for-the-badge&labelColor=black&logo=sqlite&logoColor=white)
![Bash Badge](https://img.shields.io/badge/Bash-4EAA25?style=for-the-badge&labelColor=black&logo=gnubash&logoColor=white)
![HTML Badge](https://img.shields.io/badge/HTML-E34F26?style=for-the-badge&labelColor=black&logo=html5&logoColor=white)
![CSS Badge](https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&labelColor=black&logo=css3&logoColor=white)

### Technologies:

![PostgreSQL Badge](https://img.shields.io/badge/PostgreSQL-4169E1?style=for-the-badge&labelColor=black&logo=postgresql&logoColor=white)
![GraphQL Badge](https://img.shields.io/badge/GraphQL-E10098?style=for-the-badge&labelColor=black&logo=GraphQL&logoColor=white)
![Docker Badge](https://img.shields.io/badge/docker-2496ED?style=for-the-badge&labelColor=black&logo=docker&logoColor=white)
![NodeJS Badge](https://img.shields.io/badge/NodeJS-339933?style=for-the-badge&labelColor=black&logo=Node.js&logoColor=white)
![Proxmox Badge](https://img.shields.io/badge/Proxmox-E57000?style=for-the-badge&labelColor=black&logo=proxmox&logoColor=white)
![Apache Badge](https://img.shields.io/badge/apache-D22128?style=for-the-badge&labelColor=black&logo=apache&logoColor=white)
![NGINX Badge](https://img.shields.io/badge/nginx-269539?style=for-the-badge&labelColor=black&logo=nginx&logoColor=white) 
![Git Badge](https://img.shields.io/badge/Git-F05032?style=for-the-badge&labelColor=black&logo=git&logoColor=white) 

### Tools:

![Wireshark Badge](https://img.shields.io/badge/Metasploit-3776AB?style=for-the-badge&labelColor=black&logo=monster&logoColor=white) 
![Burp Suite Badge](https://img.shields.io/badge/BurpSuite-E57000?style=for-the-badge&labelColor=black&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAYAAAA9zQYyAAAJdElEQVR42u3dBWwdV9qH8S9OpDIKvmVmpjIzMzMaytyGme0wMzMtOdowuhtmZnLDDrpmv/uXzvI6597WGfnM7fNKP1Exnsd05syZ/zOzlAHwQQBBAwQNEDRA0CBogKABggYIGiBoEDRA0DEBgmaSn+uvv6GWfF8ekbYyRQ7Kt/7x91jGb78lB2WKtJVH5PtSi49g0kPQEcRbW34qz0hHmSPHxP5bFUFbFY7JHOkoz8hPpTYfaYKOIt4z5DeSLj1lgRSJeXiDTlKRLJCeki6/kTO4IgT9WeI9R66S12SgLJcyb7ARBO1RJstloLwmV8k5BG0ErdAulBvlPRkh66VCzKPGgvaokPUyQt6TG+VCgq5CCsX7JbldGsh42SbmF4+gPbbJeGkgt8uXCDqe8X5DHpDmkiv5Yn6pFbRHvuRKc3lAvkHQ4YSbJj+Qx6WdTJPD3rgIuiqHZZq0k8flB5JG0NHGW0d+Ic9KV5krJ6oXGkF7nJC50lWelV9IHYL+/Mtkl0im9JbFUiQWPYL2KJLF0lsy5RI5g6D/M97z5Gp5UwbJKikXCwdBe5TLKhkkb8rVcl7qB+3ivVhulg9ktGyQSrH4IWiPStkgo+UDuVkujm/QLt4vy13SSCbKTrHUR9AeO2WiNJK75MvhBe3i/Y48KK0kV/aJgaCTsE9ypZU8KN+JNOgqlsl+JE9KtsyUAjEQ9GlUIDMlW56UH0naaQlaF+NF6SZ5UihWAwgahZIn3eTF6gRtIOjQEDRBexB0KiLoDplm2elmb1xN0CmAoJfNNDNNZYXZJ1vNFuSaje9s1vk1RX4NQccMQa+Ybaecnu8RNAiaoAmaoAmaoAmaoAmaoEHQBI24BL1+IUGnEILeuJSgQdAEHQqCJmgQNEGDoAk6egRN0CBoggZBE3QgCJqgQdAETdAB2LqSoFMEQU/sZhqCTgEEPSbHNASdAgh6RBvTEHT0CDqYmPO3mL17E0Ej4KCHt0o+5vdu5lwOBBz0gIbu7I1Es32N2dvXc9AMAg66z0fJx/zWdZychICD7v6OWXlZ3GMWgibonAyz0uIUiJmgCbr1M2ZFJy3h7N4UeMwETdBNHzE7cSQuqxkETdAede8yO7LfEs6B3UHHTNAE7QLdtyO5mD+6I+B4CZqgX7/K/XLnH/fVu97dAYdL0AT96uVmaz62hHPymPv5OthoCZqgM39n9tc/W4Jxy3dtnw88WoIm6MkDLMG4GytdXg88WIIm6MSbjdwt7371Ao+VoAm6+9su1kQzOjvwUAmaoFs+ZVZSZAnnL0MCj5SgCfqjO82OHrSEs2Cy+4WRoAk62KDfvMZs90ZLOOsXuaW8YOMkaILOusRsdV6KbDYiaIKePtISTsE+sw9Dv6VN0AQ9rJUlNYunmI3p4P7+gY3cU9udXjXLftmsxRNmDe837eFwj1hlXUrQqIGgczLcjZEopqLC7NMTZgX7zfbvcj+fb15utm6BeztWq2cIGqcx6Ab3uf0XkQ7nckSPoN2KRv5Wi3YIOnoE7daPl8+yCIagawBB/7GPBTEEjWoH3THLrPC4+2WtspKgCTquQft/BNFSm/POjWb17nEaPeCW40R7nd2KiFi3t1yMouU7s8FNnTEdnLEdzf7Uxzn0CUGDE/wJOgAETdAgaIIGQRN05AiaoEHQBA2CJmiCJmiCJmiCJmiCJmgQNEGDoAk6FARN0CBotytQbnC7AUU7AZ2cDKfHu+7fO6iJ2wE4OtvtABzf2eyVyyIMGgS9ebl7WHbVPPf3bVnhHqTN3+K2nRbsd/u2iwqtGuOO+u2YxVfo6BB0BOM56vcNfuQIGkEnH3OPd/gZOhYIOvG51b0+SJFfCgmaoAc2ZpUjcARdVprcKU8j27FsFxmCdqsURw6Y7d3uXg23fqHZsplmH//JbMYos9z+ZhO6mA1vbTagoXshfk6GW46rf6/ZuzeZvXqFe2g30RtsJ3VnHZqgY+D9W8wO5pt3pg7jxgpBx8AbV5vtWGfeyfuju/FC0AQdMHcs75o8886KOe7Adm59E3Tw8v5g3tm0zL22mb0cBB08nbzknT2b/a/HIGiCDobWkb1TsN+9LYDddgQdvJx0/3rzpyf8L8knaIIOhFtrLjzmv7mSnc5+aIIOnjv5dP9O8457rzhBE3TgXr3crVj4ZlIPnlgh6JjQbXDvzP8Dj2ARdEyM62TeWbfQfQUnaIIOXpc33N7lU82+He45Qh6SJejgNXrQLcGdak4ec2+n5alvgg6evurqq6//8amcDI4xIOjguY1Eq/PMO8NacS4HQcfElCHmHW3656CZeCDovvXMOxsWuy2jBE3QwWv+uFlJkZ1yDuW7u4UcBRY4gnbHdx3cY6ccha7gOdsuBgg665LET530+YjDGmOCoCcPTP7hVoIOGkH3/sC8s3GJ53lAgibo+Gw6ck+dvHdLipwPDYIe0IgDz1MGQXOCPwiaoAmaoAmaoAmaoAmaoEHQBA2CJujoETRBg6AJGgRN0ARN0ARN0ARN0ARN0CBoggZBE3QwCLq02KzjKwSNGAZdVGi2ZYXZnPFmw1q6p7vd2RsEjZgF/dZ11XghJkETdOohaIIGQRM0CNohaIJ+UbpJnhSK1QCCRqHkSTd58XMH/e+jC5MmP5InJVtmSoEYCPo0KpCZki1Pyo8k7d9brGbQnnGhf0celFYyWfaJJYGgsU8mSyt5UL7jSS36oD2Rf1nukkYyUXYS9Bc+6J0yURrJXfJlf0WBBO2J/GK5WT6Q0bJBKgk65VTKBhktH8jNcrG/jpgF7Yn8PLla3pTBskrKCTo2ymWVDJY35Wo5L+puggzaE/kZcolkSm9ZLEUEXeOKZLH0lky5RM7wXEqC9kReR34hz0pXmSsnCDoyJ2SudJVn5RdSp7rXkaD9kafJD+RxaS/T5DBBf2aHZZq0l8flB26ZLPoh6ORC/4Y8IM0lV/IJ2pF8yZXm8oB8I9TrSND+yL8kt0sDGS/bvgBBb5Px0kBuly/F/ToStD/yC+VGeV9GyHqpiGHQFbJeRsj7cqNcGMNLQtARRH6OXCWvyUBZLmUBBV0my2WgvCZXyTkpflkIOoJlxN9IuvSUBVIUddBSJAukp6TLb6JaJiNoIq8tP5VnpaPMkWPVCPqYzJGO8qz8VGrzka7JoIm8lnxfHpG2MkUOVhH0QZkibeUR+b7U4iMYWdDhA8IPGiBogKBB0ABBAwQNEDRA0CBogKABggYIGvgb86DDgncoNacAAAAASUVORK5CYII=)
![Wireshark Badge](https://img.shields.io/badge/Wireshark-1679A7?style=for-the-badge&labelColor=black&logo=wireshark&logoColor=white) 
![Nmap Badge](https://img.shields.io/badge/Nmap-220a37?style=for-the-badge&labelColor=#220a37&logo=)  

### Miscellaneous:

![GNU/Linux Badge](https://img.shields.io/badge/GNU\/Linux-F05032?style=for-the-badge&labelColor=black&logo=linux&logoColor=white)
![Autodesk Inventor Badge](https://img.shields.io/badge/Inventor-E57000?style=for-the-badge&labelColor=black&logo=autodesk&logoColor=white) 
![LaTex Badge](https://img.shields.io/badge/LaTex-008080?style=for-the-badge&labelColor=black&logo=LaTex&logoColor=white)


## Projects:


### Dotfiles:

- My system configurations which include expansive configuration files for a large variety of tools and programs that I regularly use. 
- Includes bash scripts which allow for the automated configuration and installing of dependencies, automatically adjusting to the system and it's package manager.
[Check out my dotfiles here!](https://git.tayari.gg/tayari/dotfiles)


### Nmap2Tex:

- Developed a tool which automatically takes in outputs from Nmap scans and converts them into nicely formatted network diagrams in LaTeX.
- Supports including users in the network inventory.
- Allows for the input of Nmap vulnerability scans resulting in tables displaying all the CVEs present on hosts and correspondingly rating and colour coding how vulnerable a host is.

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=daylamtayari&repo=Nmap2Tex&show_icons=true&theme=great-gatsby)](https://github.com/daylamtayari/Nmap2Tex)

### Curated ICO:

- Working in a coordinated team environment to develop a data aggregation and analysis platform for the crypto space.
- Designed an SQL database schema to meet specific requirements and deployed it to a PostgreSQL database.
- Built and configured a GraphQL API to allow interactions with the PostgreSQL database.
- Developed an automated data retrieval tool which retrieves data from a variety of data sources, sanitise and validate the data and then input it into a database utilising a GraphQL API.


### To-Do Export:

- Covered a gap in the Microsoft To-Do program by devising a unique and custom solution which exports task lists into a format compatible with other task management applications.
- Developed in Java and utilises REST APIs to retrieve the task lists which are then convereted into appropriate CSV and JSON formats.
- Was developed following a personal need to export my task lists from Microsoft To-Do to another task management application and there were no guides, tools or in-app feature that allowed me to export my task lists, let alone in a format that would be compatible to be imported in other task management applications.

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=daylamtayari&repo=Microsoft-To-Do-Export&show_icons=true&theme=great-gatsby)](https://github.com/daylamtayari/Microsoft-To-Do-Export)

### Twitch Recover:

- Developed a Java software application which offers numerous tools to assist users in the handling and recovery of video content for a popular livestreaming platform.
- Utilisied REST and GraphQL APIs to retrieve and feed video content to end users.
- Resolved hundreds of user tickers and performed the relevant support and issue remediation.
- The tool received a total of over **90,000 downloads**.

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=TwitchRecover&repo=TwitchRecover&show_icons=true&theme=great-gatsby)](https://github.com/TwitchRecover/TwitchRecover)

### Discord Bots:

For various reasons, I have created numerous Discord bots, built in JavaScript using NodeJS and the `Discord.js` NPM package.
- [Topic Bot](https://git.tayari.gg/tayari/Topic-Bot): Discord bot which following a particular message being sent in chat, posts a topic or conversation prompt, taken from a given list of prompts. Also allows users to both control the bot and add new prompts directly through Discord, allowing them to not have to go manually edit the files every time. 
- [Discord Embedder](https://git.tayari.gg/tayari/discord-embedder): Discord bot which takes in a given input and creates and posts the content in an embed format.


### Pi-Hole Blocklist:

Created an extensive blocklist for the DNS sinkhole Pi-Hole, compiled from numerous other blocklists and then further customised by myself.

[![ReadMe Card](https://github-readme-stats.vercel.app/api/pin/?username=daylamtayari&repo=Pi-Hole-Blocklist&show_icons=true&theme=great-gatsby)](https://github.com/daylamtayari/Pi-Hole-Blocklist)
